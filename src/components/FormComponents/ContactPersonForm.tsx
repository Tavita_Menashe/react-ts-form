import {Input} from "../../styles/Input";
import Select from "@mui/material/Select";
import MenuItem from '@mui/material/MenuItem';
import Grid from "@mui/material/Grid";
import Checkbox from "@mui/material/Checkbox";
import {FormTitle, FormWrapperStyled} from "../../styles/Form";

type AccountData = {
    name: string,
    surname: string,
    jobTitle: string,
    email: string
}

type AccountFormProps = AccountData & {
    updateFields: (fields: Partial<AccountData>) => void
}

export function ContactPerson({name, surname, jobTitle, email, updateFields}: AccountFormProps) {
    return (
        <>
            <FormTitle>Company</FormTitle>
            <FormWrapperStyled>
                <Input
                    required
                    type="text"
                    value={name}
                    placeholder="Name"
                    onChange={e => updateFields({name: e.target.value})}
                />
                <Input
                    required
                    type="text"
                    placeholder="Surname"
                    value={surname}
                    onChange={e => updateFields({surname: e.target.value})}
                />
                <Input
                    required
                    type="text"
                    placeholder="Job title"
                    value={jobTitle}
                    onChange={e => updateFields({jobTitle: e.target.value})}
                />
                <Input
                    autoFocus
                    required
                    type="email"
                    placeholder="E-mail address"
                    value={email}
                    onChange={e => updateFields({email: e.target.value})}
                />

                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Select
                            style={{
                                width: "100%"
                            }}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Age"
                        >
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                            <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </Grid>
                    <Grid item xs={8}>
                        <Input
                            autoFocus
                            required
                            type="email"
                            placeholder="E-mail address"
                            value={email}
                            onChange={e => updateFields({email: e.target.value})}
                        />
                    </Grid>
                </Grid>

                <Grid container spacing={2}>
                    <Grid item xs={2}>
                        <Checkbox defaultChecked/>
                    </Grid>
                    <Grid item xs={8}>
                        <p>Lorem ipsumsaepe sapiente, tempora veniam volu fuga iste iure laborum modi Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, temporibus! non possimu Aspernatur dignissimos et iste iusto mollitia nisi numquam omnis quis sequi, temporibus? Totam!. Ab ad at consequatur expedita molestiae nulla soluta! Aut delectus dolor incidunt, laudantium odio possimus quaerat quod reprehenderit vero? Accusantium, animi asperiores aut corporis eaque!
                        </p>
                    </Grid>
                </Grid>
            </FormWrapperStyled>
        </>
    )
}


