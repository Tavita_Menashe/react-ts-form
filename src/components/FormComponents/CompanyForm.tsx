import {FormWrapper} from "./FormWrapper";
import {Input} from "../../styles/Input";
import {FormTitle} from "../../styles/Form";

type CompanyData = {
    companyCode: number,
    companyName: string
}

type CompanyProps = CompanyData & {
    updateFields: (fields: Partial<CompanyData>) => void
}

export function CompanyForm({companyCode, companyName, updateFields}: CompanyProps) {
    return (
        <>
            <FormTitle>Company</FormTitle>
            <FormWrapper>
                <Input
                    autoFocus
                    required
                    type="number"
                    placeholder="Company code"
                    value={companyCode}
                />
                <Input
                    required
                    type="text"
                    value={companyName}
                    placeholder="Company name"
                    onChange={e => updateFields({companyName: e.target.value})}
                />
            </FormWrapper>
        </>
    )
}
