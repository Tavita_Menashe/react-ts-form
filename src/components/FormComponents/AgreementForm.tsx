import {FormWrapper} from "./FormWrapper";

type AgreementData = {
    agreement: string
}

type AgreementProps = AgreementData & {
    updateFields: (fields: Partial<AgreementData>) => void
}

export function AgreementForm( { agreement }: AgreementProps ) {
    return (
        <FormWrapper title="ContactPerson">
            <div>
                {agreement}
            </div>
        </FormWrapper>
    )
}
