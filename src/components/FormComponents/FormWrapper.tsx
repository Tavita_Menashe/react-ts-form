import { ReactNode } from "react";
import {  FormBody, FormWrapperStyled } from "../../styles/Form";


type FormWrapperProps = {
    title: string
    children: ReactNode
}
export function FormWrapper({  children }: FormWrapperProps) {
    return (
        <>
            {/*<FormTitle>*/}
            {/*    {title}*/}
            {/*</FormTitle>*/}
           <FormWrapperStyled>
               <FormBody
                   // style={{
                   //     display: "grid",
                   //     gap: "1rem .5rem",
                   //     justifyContent: "flex-start",
                   //     gridTemplateColumns: "auto minmax(auto, 400px)",
                   // }}
               >
                   {children}
               </FormBody>
           </FormWrapperStyled>
        </>
    )
}
