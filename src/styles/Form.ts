import styled from "styled-components";

export const Form = styled.form`
  margin: 10px 50px 50px 10px;
`

export const FormTitle = styled.h5`
  font-size: 18px;
  font-weight: 500;
`

export const FormBody = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`
export const FormButtons = styled.div`
    display: flex;
  justify-content: space-between;
  margin: 50px;
`

export const FormWrapperStyled = styled.div`
  border: 1px solid green;
  margin: 50px;
`
