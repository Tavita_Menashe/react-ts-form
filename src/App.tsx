import {useState} from "react";
import {useMultistepForms} from "./useMultistepForms";
import {CompanyForm} from "./components/FormComponents/CompanyForm";
import {ContactPerson} from "./components/FormComponents/ContactPersonForm";
import {AgreementForm} from "./components/FormComponents/AgreementForm";
import GlobalStyles from "./styles/GlobalStyles"
import {PrimaryButton, OutlinedButton} from "./styles/Button"
import {Container, FormContainer} from "./styles/Container.styled"
import {Form, FormButtons} from "./styles/Form";

type FormData = {
    firstName: string
    lastName: string
    age: string
    street: string
    city: string
    state: string
    zip: string
    email: string
    password: string
}

const INITIAL_DATA: FormData = {
    firstName: "",
    lastName: "",
    age: "",
    street: "",
    city: "",
    state: "",
    zip: "",
    email: "",
    password: "",
}


function App() {
    const [data, setData] = useState(INITIAL_DATA);

    function updateFields(fields: Partial<FormData>) {
        setData(prev => {
            return {...prev, ...fields}
        })
    }

    const {step, currentStepIndex, isFirstStep, isLastStep, next, back} = useMultistepForms([
        <CompanyForm updateFields={updateFields}/>,
        <ContactPerson updateFields={updateFields}/>,
        <AgreementForm updateFields={updateFields}/>,
    ])
    return (
        <>
            <GlobalStyles/>
            <Container>
                <FormContainer>
                    <Form>
                        {step}
                        <FormButtons>
                            {!isFirstStep && <OutlinedButton type="button" onClick={back}>Back</OutlinedButton>}
                            <PrimaryButton style={isLastStep ? {display: "none"} : {display: "flex"}} type="button"
                                           onClick={next}>
                                Next
                            </PrimaryButton>
                        </FormButtons>
                    </Form>
                </FormContainer>
            </Container>
        </>
    )
}

export default App;


